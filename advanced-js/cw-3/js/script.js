"use strict"

// let a = 5;
//
// function first() {
//     let b = 10;
//
//     function second() {
//         let c = 15;
//
//         function third() {
//             return c + b + a;
//         }
//
//         c = 0;
//
//         return third();
//     }
//
//     return second();
// }
//
// console.log(first());



// function globTest(num, obj) {
//     let array = [1, 2, 3];
//
//     function doSome(i) {
//         num += i;
//
//         array.push(num);
//         console.log(`num: ${num}`);
//         console.log(`array: ${array}`);
//         console.log(`obj.value: ${obj.value}`);
//         console.log("---------------");
//     }
//
//     return doSome;
// }
//
// let refObj = { value: 10 };
//
// let foo = globTest(2, refObj);
// let bar = globTest(6, refObj);
//
// bar(2);
// refObj.value++;
// foo(4);
// bar(4);


// let login = document.querySelector(".login");
// let btn = document.querySelector(".btn");
//
// const admin = "Vasia";
//
// btn.addEventListener('click', () => {
//     try {
//         if (login.value !== admin) {
//             throw new Error('Go out!');
//         } else {
//             console.log('Hello, admin!');
//         }
//     } catch (error) {
//         console.log(error);
//     }
// });


let roles = [
    "admin",
    "user",
    "guest"
];

function RoleValidate(mess) {
    this.mess = mess;
    this.name = "Role validate";
}

let test = [
    {
        name: 'John',
        role: 'admin',
        age: 12
    },
    {
        name: 'Ivan',
        role: 'user',
        age: 18
    },
    {
        name: 'Alex',
        role: 'guest',
        age: 40
    },
    {
        name: 'Ann',
        role: '',
        age: 10
    },
    {
        name: 'Julie',
        role: 'user'
    }
];

function validate(obj) {
    try {
        switch (obj) {
            case (obj.role == "admin"):
                console.log(11)
            throw new RoleValidate("role is wrong");
            break;
        }
    } catch (e) {
        console.log(e);
    }
}

test.map(elem => {
    validate(elem);
});