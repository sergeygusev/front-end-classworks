"use strict"

class Car {

    constructor(model, color, volume) {
        this.model = model;
        this.color = color;
        this.volume = volume;
        this.maxSpeed = 150;
    }

    startEngine() {
        console.log("Brum Brum");
    }

}

class Minivan extends Car {
    constructor(model, color, volume, doors) {
        super(model, color, volume);
        this.doors = doors;
    }
}

let MyCar = new Car('Audi', 'Black', 2.0);
let NewMinivan = new Minivan('Smart', 'White', 1.4, 5);

MyCar.startEngine();
console.log(MyCar.model);
console.log(MyCar.color);
console.log(MyCar.volume);
console.log("----------------");
NewMinivan.startEngine();
console.log(NewMinivan.model);
console.log(NewMinivan.color);
console.log(NewMinivan.volume);
console.log(NewMinivan.doors);