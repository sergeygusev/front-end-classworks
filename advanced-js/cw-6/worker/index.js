"use strict"

class Worker {

    constructor(name, surname, rate, days) {
        this._name = name;
        this._surname = surname;
        this._rate = rate;
        this._days = days;
    }

    get getName() {
        return this._name;
    }

    get getSurname() {
        return this._surname;
    }

    get getRate() {
        return this._rate;
    }

    get getDays() {
        return this._days;
    }

    get getSalary() {
        return this._rate * this._days;
    }

}

let worker = new Worker("Иван", "Иванов", 10, 31);

console.log(worker.getName);
console.log(worker.getSurname);
console.log(worker.getRate);
console.log(worker.getDays);
console.log(worker.getSalary);