"use strict"

const HORIZONTAL_CORDS_VALUES = [0, 1, 2];
let pers = null;

function hidePers() {
    let userRow = document.querySelector(".user-row");

    Array.from(userRow.children).forEach((element) => {
        element.innerHTML = "";
    });
}

function appearPers(cord) {
    let userRow = Array.from(document.querySelector(".user-row").children);

    console.log();
    if (HORIZONTAL_CORDS_VALUES.includes(cord)) {
        userRow[cord].insertAdjacentHTML(
            "afterbegin",
            ' <img src="game/img/pers.jpg" alt="" />'
        );
    }
}

function startGame() {
    hidePers();
    pers = new Mario();
    pers.cords = 1;
}

class Mario {
    constructor(cords) {
        this._cords = cords;
    }

    get cords() {
        return this._cords;
    }
    set cords(value) {
        this._cords = value;
        hidePers();
        appearPers(this._cords);
    }

    goLeft() {
        if (this._cords !== 0) {
            this._cords = this._cords - 1;
            hidePers();
            appearPers(this._cords);
        }
    }

    goRight() {
        if (this._cords !== 2) {
            this._cords = this._cords + 1;
            hidePers();
            appearPers(this._cords);
        }
    }
}

class Barier {

    constructor(cordX, cordY) {
        this._cordX = cordX;
        this._cordY = cordY;
    }

}

function persMove(vector) {
    if (vector === 'ArrowLeft') {
        pers.goLeft();
    } else if (vector === 'ArrowRight') {
        pers.goRight();
    }
}

document.addEventListener("DOMContentLoaded", () => {
    startGame();
});

document.addEventListener("keydown", (e) => {
    persMove(e.key);
});
