"use strict"

// function test() {
//     console.log(this)
// }
//
// test();
//
//
// let obj = {
//     name: 'Vasia',
//     lastName: 'Pupkin'
// }

let Car = function (type, engine = true, wheels = 4, doors = 4, color, startEngine) {
    this.type = type;
    this.engine = engine;
    this.wheels = wheels;
    this.doors = doors;
    this.color = color;
}

Car.prototype.startEngine = function () {
    if (this.engine) {
        console.log(this.type + " Brum");
    }
};

let minivan = new Car(
    'minivan',
    true,
    4,
    5,
    "#000",
)

// console.log(minivan);
// minivan.startEngine();

// let minivan = {
//     type: "minivan",
//     engine: true,
//     wheel: 4,
//     doors: 5,
//     color: "#000",
//     startEngine: function () {
//         if (this.engine) {
//             console.log(this.type + "Brum");
//         }
//     }
// }
//
// let sedan = {
//     type: "sedan",
//     engine: true,
//     wheel: 4,
//     doors: 4,
//     color: "#fff",
//     startEngine: function () {
//         if (this.engine) {
//             console.log(this.type + "Brum");
//         }
//     }
// }


let Worker = function (name, surname, rate, days) {
    this.name = name;
    this.surname = surname;
    this.rate = rate;
    this.days = days;
};

Worker.prototype.getSalary = function () {
    return this.rate * this.days;
};

Worker.prototype.age = 20;

let worker = new Worker('Иван', 'Иванов', 10, 31);

// console.log(worker.name);
// console.log(worker.surname);
// console.log(worker.rate);
// console.log(worker.age);
// console.log(worker.days);
// console.log(worker.getSalary());




let MyString = function () {};

MyString.prototype.reverse = function (str) {
    return str.split("").reverse().join("");
};

MyString.prototype.ucFirst = function (str) {
    return `${str[0].toUpperCase()}${str.slice(1)}`;
};

MyString.prototype.ucWords = function (str) {
    let newStr = str.split(" ").map(el => {
        return this.ucFirst(el);
    });
    return newStr.join(' ');
};

let str = new MyString();

console.log(str.reverse("abcde"));
console.log(str.ucFirst('abcde'));
console.log(str.ucWords('abcde abcde abcde'));




