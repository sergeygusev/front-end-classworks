"use strict"

// function a(par) {
//     Array.from(arguments).forEach(elem => {
//         console.log(elem);
//     });
// }
//
// function b(must, ...rest) {
//     console.log(must);
//     rest.forEach(elem => {
//         console.log(elem);
//     })
// }

// a(1, 2, 3, 4, 5, 6);
// b(6, 5, 4, 3, 2, 1);



// const array1 = [1, 2, 3, 4];
// const array2 = [5, 6, 7, 8];
// const array3 = [9, 10, 11, 12];
//
// const array4 = [...array1, ...array2, ...array3];
//
// console.log(array4);



// const test = {
//     name: 'Vlad',
//     role: 'Admin',
//     id: 1234567,
//     command: [
//         123,
//         2342,
//         45546
//     ],
//     testObj: {
//         name1: 'Vlad',
//         role1: 'Admin'
//     }
// }
//
// const {name, role, id, testObj: {name1, role1}, ...command} = test;
//
// console.log(command, name1, role1);


// const str = 'asdasdasdasd';
// const test = [...str];
//
// new Set(arr);
// new Map(arr);
// NodeList


const obj = [
    {
        field1: 1,
        field2: 2,
        field3: 3,
        field4: 4,
    },{
        field1: 1,
        field2: 2,
        field3: 3,
        field4: 4,
    },{
        field1: 1,
        field2: 2,
        field3: 3,
        field4: 4,
    },{
        field1: 1,
        field2: 2,
        field3: 3,
        field4: 4,
    },{
        field1: 1,
        field2: 2,
        field3: 3,
        field4: 4,
    },{
        field1: 1,
        field2: 2,
        field3: 3,
        field4: 4,
    }
];

let array = [];

obj.forEach((elem) => {
    let {field1, field2} = elem;
    array.push({
        "field1": field1,
        "field2": field2,
    });
});

console.log(array);