const inputLogger = require("./input");

describe('Test input logic', () => {
    test('Check input value', () => {
        const testData = {
            target: {
                value: '123'
            }
        }
        expect(inputLogger(testData)).toBe("123");
    });
});