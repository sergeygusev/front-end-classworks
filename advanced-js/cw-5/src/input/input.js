function inputLogger(e) {
    const numberTemplate = "/^/+380\d{9}/";

    let value = e.target.value;

    if (numberTemplate.test(value)) {
        return value;
    }

    console.log(value);
    return 'Not a number';
}

document.addEventListener("DOMContentLoaded", () => {
    const input = document.querySelector(".input");
    input.addEventListener('keydown', inputLogger);
})

module.exports = inputLogger;



