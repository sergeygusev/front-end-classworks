"use strict"

// let data = JSON.stringify({
//     "userId": 1,
//     "id": 1,
//     "title": "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
//     "body": "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto"
// });
//
// function sendRequest(method, url, data = null) {
//     let xhr = new XMLHttpRequest();
//     let formatData = JSON.stringify(data);
//
//     xhr.open(method, url);
//     // xhr.setRequestHeader("Content-Type", "application/json");
//     xhr.send(formatData);
//
//     return xhr;
// }
//
// let response = sendRequest("GET", "https://jsonplaceholder.typicode.com/posts");
// // let response = sendRequest("POST", "https://jsonplaceholder.typicode.com/posts/", data);
// // response.replace(/"([^"]*)"/g, '«$1»');
//
// console.log(response);


let resp = fetch("https://jsonplaceholder.typicode.com/posts/1");

resp
    .then(response => {
    return response.json();
})
    .then( data => {
    console.log(data);
})

