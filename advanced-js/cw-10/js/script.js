"use strict"

// function callAPI(url) {
//     fetch(url)
//         .then(data => {
//         return data.json();
//     })
//         .then(data => {
//             console.log(data)
//         });
// };

// async function callAPI(url) {
//     let response = await fetch(url);
//     console.log(response);
// };
//
// callAPI("https://jsonplaceholder.typicode.com/users");

// callAPI("http://api.ipify.org/?format=json");
// callAPI("http://ip-api.com/json/24.48.0.1");

// axios.get("http://ip-api.com/json/24.48.0.1").then(data => {
//     console.log(data);
// });



let posts = null;
let users = null;

function searchUserData(id, users) {
    let data = users.filter(el => {
        if (el.id === id) {
            return {
                name: el.name,
                email: el.email,
            }
        }
    });
    return data;
}

function createCards(posts, users) {
    let cardsContainer = document.querySelector(".cards");

    posts.map(({title, body, userId}) => {
        let userData = searchUserData(userId, users);

        let template = `<div class="card"><span class="card__title">${title}</span><p class="card__text">${body}</p>
<span>${userData[0].name}</span>
<span>${userData[0].email}</span>
</div>`;
        cardsContainer.insertAdjacentHTML('beforeend', template);
    });
}

async function getUsers() {
    return await axios.get('https://jsonplaceholder.typicode.com/users');
}

async function getPosts() {
    return await axios.get('https://jsonplaceholder.typicode.com/posts');
}

Promise.all([getUsers(), getPosts()])
    .then(results => {
        users = results[0];
        posts = results[1];
        createCards(posts.data, users.data);
    })
    .catch((error) => {
        console.log(error)
    });


