"use strict"

// let testPromise = new Promise((resolve, reject) => {
//     setTimeout(() => resolve("Done"), 1000);
// });
//
// testPromise.then(value => {
//     console.log(value);
// });
//
// testPromise.catch(value => {
//     console.log(value);
// });


// function delay(ms) {
//     return new Promise((resolve, reject) => {
//         setTimeout(() => resolve(), ms);
//     });
// };
//
// delay(2000).then(() => console.log(`Выполнилось через 2 секунды`));


let xhr = new XMLHttpRequest();
xhr.open("GET", "https://swapi.dev/api/people/1");
xhr.send();
xhr.onload = () => {
    document.write(xhr.response);
}