const gulp = require("gulp");
const sass = require("gulp-sass");

// function scss() {
//     gulp.watch('scss/*.scss', () => {
//         return gulp.src('scss/*.scss').pipe(sass()).pipe(gulp.dest("css"));
//     });
// }

function scss() {
    return gulp.src('scss/*.scss').pipe(sass()).pipe(gulp.dest("css"));
}

exports.default = gulp.series(scss);