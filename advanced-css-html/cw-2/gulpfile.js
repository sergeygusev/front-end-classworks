const { src, dest, watch, series} = require("gulp");

const server = require('browser-sync').create();

function reload(cb) {
    server.reload();
    cb();
}

function html(cb) {
    return src("src/*.html").pipe(dest("dist"));
}

function serverStart(cb) {
    server.init({
        server: {
            baseDir: "src/"
        },
        notify: false,
        open: true,
        cors: true
    });

    watch("src/*.html", series(reload));
    cb();
}



exports.default = series(html, serverStart);