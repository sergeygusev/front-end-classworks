"use strict"

// let obj = {
//     name: "Viktor",
//     age: 30
// }
//
// let obj2 = obj;
//
// console.log(obj);
// console.log(obj2);



// let user = {};
//
// user.name = "John";
// user.surname = "Smith";
// user.name = "Pete";
// delete user.name;
//
// console.log(user);



// function isEmpty(object) {
//     for (let key in object) {
//         return false;
//     }
//
//     return true;
// }
//
// let shedule = {}
//
// alert(isEmpty(shedule));
//
// shedule["8:30"] = "get up";
//
// alert(isEmpty(shedule));


//========================
// 1-st task
let student = {
    "name": "Иван",
    "last name": "Сусанин",
    "laziness": 4,
    "trick": 4
};

// console.log(student);


//========================
//3-rd task
let student2 = {};

for (let key in student) {
    student2[key] = student[key];
}

if (student2.laziness >= 3 && student2.laziness <= 5 && student2.trick <= 4) {
    console.log(`Cтудент ${student2.name} ${student2["last name"]} отправлен на пересдачу`);
}

console.log(student2);


//========================
//4-th task
student2.laziness = 6;
student2.trick = 3;

if (student2.laziness >= 5 && student2.trick < 4) {
    console.log(`Cтудент ${student2.name} ${student2["last name"]} передан в военкомат`);
}

console.log(student2);






