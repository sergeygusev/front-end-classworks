import React from "react";

const ToDoList = ({list = [{text: "No items"}]}) => {
    return(
        <div className="todo-list">
            {
                list.map(({text}) => {
                    <div className="todo-list__item">
                        <span>{text}</span>
                    </div>
                })
            }
        </div>
    )
}

export default ToDoList;