import React, {Component} from "react";
import {connect} from "react-redux";

const mapStoreToProps = (store) => {
    return {
        currentUser: store.user,
        code: store.code
    };
};

class CreateNotes extends Component{
    render() {
        const {currentUser, code, dispatch} = this.props;

        return(
            <div className="">
                <input type="text" />
                <button>Create</button>
                <p>{currentUser}</p>
                <p>{code}</p>
                <button onClick={() => {
                    dispatch({type: "INCREMENT_CODE"})
                }}>Increment</button>
            </div>
        )
    }
}

export default connect(mapStoreToProps)(CreateNotes);
