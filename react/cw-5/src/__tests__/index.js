const sum = (a, b) => {
    return a + b;
}

describe('Testing sum', () => {
    test('1 + 2 must be 3', () => {
        expect(sum(1, 2)).toBe(3);
    });

    it('2 + 3 must be 5', () => {
        expect(sum(2, 3)).toBe(5);
    });
})