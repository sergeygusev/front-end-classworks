import './App.css';
import CreateNotes from "./components/CreateNotes";
import ToDoList from "./components/ToDoList";


function App() {
  return (
      <div className="container">
          <h2>Todo</h2>
          <CreateNotes/>
          <ToDoList/>
      </div>
  );
}

export default App;
