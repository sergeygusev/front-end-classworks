import React, { useState } from 'react';
import logo from './logo.svg';
import './App.scss';
import ButtonFunc from "./components/ButtonFunc";
import Box from "./components/Box";
import LoginLogic from "./components/LoginLogic";

function App() {
  const [active, setActive] = useState(false);
  const [loggedIn, setLoggedIn] = useState(false);

  return (
      <div>
        {/*<Box active={active} />*/}
        {/*<ButtonFunc boxHandler={setActive} text={"Кнопка 2"} active={active} />*/}
        <LoginLogic loggedIn={loggedIn} loggedInHandler={setLoggedIn}/>
      </div>
  );
}

export default App;
