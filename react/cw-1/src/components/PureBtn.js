import React from 'react';

const PureBtn = ({ loggedIn, loggedInHandler }) => {
    const btnClick = () => {
        loggedInHandler(!loggedIn);
    }
    if (loggedIn) {
        return <button onClick={btnClick}>Log out</button>
    } else {
        return <button onClick={btnClick}>Log in</button>
    }
}

export default PureBtn;