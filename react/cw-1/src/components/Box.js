import React from 'react';

const Box = ({active}) => {
    return(
        <div className={active ? "box box_active" : "box"}></div>
    )
}

export default Box;