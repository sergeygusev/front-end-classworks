import React from 'react';
import Title from "./Title";
import PureBtn from "./PureBtn";

const LoginLogic = ({ loggedIn, loggedInHandler }) => {
    return (
        <>
        <Title loggegIn={loggedIn}/>
        <PureBtn loggedIn={loggedIn} loggedInHandler={loggedInHandler} />
        </>
    )
}

export default LoginLogic;