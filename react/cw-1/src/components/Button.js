import React from 'react';

class Button extends React.Component {
    state = {
        active: true
    };

    btnHandler = () => {
        this.setState((state) => ({
            active: !state.active
        }))
    }

    render() {
        const { text, beautiful } = this.props;
        const {active} = this.state;
        return (
            <button className={`btn ${beautiful ? 'btn_beautiful' : ''}`} onClick={this.btnHandler}>
                {`${active}`}
            </button>
        )
    }
}

export default Button;