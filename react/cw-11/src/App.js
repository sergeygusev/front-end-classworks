import './App.css';
import ControlledLogin from "./components/ControlledLogin";

function App() {
    const handlerSubmit = (e) => {
        e.preventDefault();
    }

  return (
    <div className="App">
        <div className="login-form">

          <ControlledLogin/>
        </div>
    </div>
  );
}

export default App;
