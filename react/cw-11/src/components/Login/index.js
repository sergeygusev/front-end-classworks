import React, { useRef, useState } from "react";

const Login = () => {
    const [errors, setErrors] = useState({});

    const loginInput = useRef(null);
    const passwordInput = useRef(null);
    const confirmPasswordInput = useRef(null);

    const handlerSubmit = (e) => {
        e.preventDefault();
        const login = loginInput.current.value;
        const password = passwordInput.current.value;
        const confirmPassword = confirmPasswordInput.current.value;
        console.log(login, password, confirmPassword);

        const validationErrors = {};

        const EMAIL_REGEX = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;

        if (!EMAIL_REGEX.test(login)) {
            validationErrors.login = 'This is not a valid e-mail';
        }

        if (!login) {
            validationErrors.login = 'This field is required';
        }

        if (!password) {
            validationErrors.password = 'This field is required';
        }

        if (!confirmPassword) {
            validationErrors.confirmPassword = 'This field is required';
        }

        if (password !== confirmPassword) {
            validationErrors.confirmPassword = 'Passwords do not match';
        }

        if (Object.keys(validationErrors).length > 0) {
            setErrors(validationErrors);
        }
    }

    return (
        <form className="form" onSubmit={handlerSubmit}>
            <input className="form-control" type="text" ref={loginInput} placeholder="Login" defaultValue="Admin" />
            <p className="error">{errors.login}</p>

            <input className="form-control" type="password" ref={passwordInput} placeholder="Type pass" />
            <p className="error">{errors.password}</p>
            <input className="form-control" type="password" ref={confirmPasswordInput} placeholder="Confirm pass" />
            <p className="error">{errors.confirmPassword}</p>
            <button className="btn btn-primary btn-lg" type="submit">Submit form</button>
        </form>
    )
}

export default Login;