import React, { useEffect, useState } from "react";
import { Field, reduxForm } from "redux-form";

const EMAIL_REGEX = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;

let ControlledLogin = (props) => {
    const [errors, setErrors] = useState({});
    const [values, setValues] = useState({ login: "Admin", password: "", passwordConfirm: "" });

    // useEffect(() => {
    //     validateForm();
    // }, [values]);

    const handlerChange = (e) => {
        const { name, value } = e.target;

        setValues({ ...values, [name]: value });
    }



    const requiredInput = (input) => !input && "Field required";
    const isEmail = (input) => !EMAIL_REGEX.test(input) && 'Please enter a valid email';
    const passwordMatch = (input, values) => input !== values.password && "Password do not match";

    return (
        <form
            className="form"
            onSubmit={props.onSubmit}
        >
            <Field
                className="form-control"
                type="text"
                placeholder="Login"
                name="login"
                component="input"
                validate={[requiredInput, isEmail]}
                onChange={handlerChange}
                value={values.login}
            />
            {/*<p className="error">{errors.login}</p>*/}

            <Field
                className="form-control"
                type="password"
                placeholder="Type pass"
                name="password"
                component="input"
                onChange={handlerChange}
                value={values.password}
            />
            {/*<p className="error">{errors.password}</p>*/}

            <Field
                className="form-control"
                type="password"
                placeholder="Confirm pass"
                name="passwordConfirm"
                component="input"
                onChange={handlerChange}
                value={values.passwordConfirm}
            />
            {/*<p className="error">{errors.passwordConfirm}</p>*/}

            <button className="btn btn-primary btn-lg" type="submit">Submit form</button>
        </form>
    )
}

export default ControlledLogin = reduxForm({
    form: 'Login'
})(ControlledLogin);