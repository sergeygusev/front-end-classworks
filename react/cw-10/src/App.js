import './App.css';
import LoginPage from './components/LoginPage/LoginPage';
import { Switch, Route } from 'react-router-dom';

function App() {
  return (
    <div>
        <Switch>
            <Route path="/login">
                <Login></Login>
            </Route>
            <Route path="/">
                <div>Login Page</div>
            </Route>
        </Switch>
    </div>
  );
}

export default App;
