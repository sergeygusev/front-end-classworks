import React from 'react';
import './App.css';
import Footer from './components/Footer/Footer';
import Header from './components/Header/Header';
import axios from 'axios';
import Loader from './components/Loader/Loader';
import Sidebar from './components/Sidebar/Sidebar';
import AppRoutes from './routes/AppRoutes';

class App extends React.Component {
  state = {
    emails: [],
    isLoading: true,
    user: null
  }

  componentDidMount() {
    setTimeout(() => {
      axios('/emails.json')
        .then(res => {
          this.setState({ emails: res.data, isLoading: false })
        })
    }, 1000)
  }

  render() {
    const { emails, isLoading, user } = this.state;

    if (isLoading) {
      return <Loader />
    }

    return (
      <div className="App">
        <Header user={user} />
        <Sidebar />
        <AppRoutes emails={emails} loginUser={this.loginUser} user={user} />
        <Footer />
      </div>
    );
  }

  loginUser = (user) => {
    this.setState({user})
  }
}

export default App;
