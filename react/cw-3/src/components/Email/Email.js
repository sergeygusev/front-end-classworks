import React, { PureComponent } from 'react'
import './Email.scss'
import PropTypes from 'prop-types'
import Button from '../Button/Button';
import { Link, withRouter } from 'react-router-dom';

class Email extends PureComponent {
  render() {
    const { email, toggleFavorites, showFull } = this.props;

    return (
      <div className='email'>
        <div>
          <Link to={`/emails/${email.id}`}>{email.topic}</Link>
          <Button color='black' onClick={() => toggleFavorites(email.id)}>Toggle favorites</Button>
        </div>
        {showFull && <div>{email.body}</div>}
      </div>
    )
  }

  // static propTypes = {...}

  // static defaultProps = {...}
}

// object, string, number, bool, func, symbol, array
// .isRequired
// shape(), exact(), arrayOf(), oneOf(), oneOfType(), instanceOf()
Email.propTypes = {
  email: PropTypes.shape({
    id: PropTypes.number.isRequired,
    topic: PropTypes.string.isRequired,
    body: PropTypes.string
  }).isRequired,
  toggleFavorites: PropTypes.func.isRequired
  // title: PropTypes.string
}

export default withRouter(Email)