import React, { PureComponent } from 'react'

class Favorites extends PureComponent {
  render() {
    const { favorites } = this.props;

    return (
      <h4>Favorites: {favorites.length}</h4>
    )
  }
}

export default Favorites