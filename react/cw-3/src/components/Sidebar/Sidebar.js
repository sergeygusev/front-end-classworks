import React, { PureComponent } from 'react'
import { NavLink } from 'react-router-dom';
import './Sidebar.scss';

class Sidebar extends PureComponent {
  render() {
    return (
      <ul>
        <li><NavLink exact to="/inbox" activeClassName='link__active'>Inbox</NavLink></li>
        <li><NavLink exact to='/sent' activeClassName='link__active'>Sent</NavLink></li>
        <li><NavLink exact to='/favorites' activeClassName='link__active'>Favorites</NavLink></li>
        <li><NavLink exact to='/login' activeClassName='link__active'>Login</NavLink></li>
      </ul>
    )
  }
}

export default Sidebar