import React, { PureComponent } from 'react'
import Button from '../Button/Button'

class Footer extends PureComponent {
  render() {
    return (
      <>
        <h2>Footer</h2>
        <div>
          <Button>Click me</Button>
          <Button size={20}>Click me 2</Button>
          <Button color='green'>Click me 3</Button>
        </div>
      </>
    )
  }
}

export default Footer