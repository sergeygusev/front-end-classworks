import React, { PureComponent } from 'react'
import PropTypes from 'prop-types';

class Button extends PureComponent {
  render() {
    const { children, onClick, color, size, type } = this.props;

    return (
      <button type={type} onClick={onClick} style={{
        color, 
        fontSize: `${size}px`
      }}>{children}</button>
    )
  }
}

Button.propTypes = {
  children: PropTypes.string.isRequired,
  onClick: PropTypes.func,
  color: PropTypes.string,
  size: PropTypes.number,
  type: PropTypes.string
}

Button.defaultProps = {
  onClick: null,
  color: 'darkgrey',
  size: 14,
  type: 'button'
}

export default Button