import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Header extends Component {
  render() {
    const { title, user } = this.props;

    return (
      <>
        <h2>Header</h2>
        <h3>{title}</h3>
        <h4>{!!user && user.login}</h4>
      </>
    )
  }
}

Header.propTypes = {
  title: PropTypes.string
}

Header.defaultProps = {
  title: 'Mail client!'
}

export default Header;