import React, { PureComponent } from 'react'
import Inbox from '../pages/Inbox/Inbox';
import { Redirect, Route, Switch } from 'react-router-dom';
import Sent from '../pages/Sent/Sent';
import FavoritesPage from '../pages/FavoritesPage/FavoritesPage';
import Page404 from '../pages/Page404/Page404';
import OneEmail from '../pages/OneEmail/OneEmail';
import Login from '../pages/Login/Login';
import ProtectedRoute from './ProtectedRoute';

class AppRoutes extends PureComponent {
  render() {
    const { emails, loginUser, user } = this.props;

    const isAuth = !!user;
    // const isAuth = true

    return (
      <Switch>
        {/* <Route path="/inbox"><Inbox emails={emails} /></Route> */}
        {/* <Route exact path="/" render={() => <Inbox emails={emails} />} /> */}
        <Redirect exact from='/' to='/inbox' />
        <ProtectedRoute exact path="/inbox" render={() => <Inbox emails={emails} />} isAuth={isAuth} />
        <ProtectedRoute exact path='/emails/:emailId' render={(routerProps) => <OneEmail emails={emails} {...routerProps} />} isAuth={isAuth} />
        <ProtectedRoute exact path="/sent" component={Sent} isAuth={isAuth} />
        <ProtectedRoute exact path="/favorites" component={FavoritesPage} isAuth={isAuth} />
        <Route exact path='/login' render={(routerProps) => <Login isAuth={isAuth} loginUser={loginUser} {...routerProps} />} />
        <Route path='*' component={Page404} />
      </Switch>
    )
  }
}

export default AppRoutes