import React, { PureComponent } from 'react'
import { Redirect, Route } from 'react-router-dom';

// class ProtectedRoute extends PureComponent {
//   render() {
//     const { isAuth, ...rest } = this.props;
//     return isAuth ? <Route {...rest} /> : <Redirect to='/login' />
//   }
// }

const ProtectedRoute = ({ isAuth, ...rest }) => isAuth ? <Route {...rest} /> : <Redirect to='/login' />

export default ProtectedRoute