import React, { PureComponent } from 'react'
import { Redirect } from 'react-router-dom'
import Button from '../../components/Button/Button'

class Login extends PureComponent {
  constructor(props) {
    super(props)

    this.loginRef = React.createRef()
    this.passwordRef = React.createRef()
  }

  render() {
    const { isAuth } = this.props;

    if (isAuth) {
      return <Redirect to='/' />
    }

    return (
      <form onSubmit={this.handleUserLogin}>
        <div>
          <input type='text' placeholder='Login' ref={this.loginRef} />
        </div>
        <div>
          <input type='password' placeholder='Password' ref={this.passwordRef} />
        </div>
        <div>
          <Button type='submit'>Log in</Button>
        </div>
      </form>
    )
  }

  handleUserLogin = (e) => {
    const { loginUser } = this.props;

    e.preventDefault()

    const login = this.loginRef.current.value
    const password = this.passwordRef.current.value

    loginUser({ login, password })
    // this.props.history.push('/')
  }
}

export default Login