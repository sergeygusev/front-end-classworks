import React, { PureComponent } from 'react'
import { Link } from 'react-router-dom'
import './Page404.scss'

class Page404 extends PureComponent {
  render() {
    return (
      <div className='page-404'>
        <h2>404</h2>
        <h3>Page not found</h3>
        <div>
          <Link to='/'>Go to home page</Link>
        </div>
      </div>
    )
  }
}

export default Page404