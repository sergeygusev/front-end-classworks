import React, { PureComponent } from 'react'
import Email from '../../components/Email/Email';

class OneEmail extends PureComponent {
  render() {
    const { emails, match } = this.props;
    const emailId = parseInt(match.params.emailId);

    const email = emails.find(e => e.id === emailId)

    return <Email email={email} showFull />
  }
}

export default OneEmail