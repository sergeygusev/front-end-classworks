import React, { PureComponent } from 'react'
import Email from '../../components/Email/Email';
import PropTypes from 'prop-types';
import Favorites from '../../components/Favorites/Favorites';

class Inbox extends PureComponent {
  state = {
    favorites: JSON.parse(localStorage.getItem('favorites')) || []
  }

  render() {
    const { favorites } = this.state;
    const { emails } = this.props;

    const emailCards = emails.map(e => <Email key={e.id} email={e} toggleFavorites={this.toggleFavorites} />)

    return (
      <>
        <h2>Inbox</h2>
        <Favorites favorites={favorites} />
        <div>
          {emailCards}
        </div>
      </>
    )
  }

  toggleFavorites = (emailId) => {
    const { favorites } = this.state;

    if (favorites.includes(emailId)) {
      this.setState({ favorites: favorites.filter(el => el !== emailId) }, () => {
        localStorage.setItem('favorites', JSON.stringify(this.state.favorites))
      })
    } else {
      this.setState({ favorites: [...favorites, emailId] }, () => {
        localStorage.setItem('favorites', JSON.stringify(this.state.favorites))
      })
    }
  }
}

Inbox.propTypes = {
  emails: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number.isRequired,
    topic: PropTypes.string.isRequired,
    body: PropTypes.string
  })).isRequired
}

export default Inbox