import React, { PureComponent } from 'react';
import { withRouter } from 'react-router-dom';
import Button from '../../components/Button/Button';
import './ErrorBoundary.scss';

class ErrorBoundary extends PureComponent {
  state = {
    hasError: false,
    errorMessage: null
  }

  componentDidCatch(error, errorInfo) {
    console.log('componentDidCatch', error, errorInfo)
    // this.setState({ hasError: true, errorMessage: error })
    // axios.post('/error', {error, errorInfo})
  }

  static getDerivedStateFromError(error) {
    return { hasError: true, errorMessage: error }
  }

  render() {
    const { children } = this.props;
    const { hasError, errorMessage } = this.state;

    if (hasError) {
      return (
        <div className='error'>
          <h2>An error has occured</h2>
          <h3>{errorMessage.message}</h3>
          <div><Button onClick={this.goHome}>Go to home page</Button></div>
        </div>
      )
    }

    return children
  }

  goHome = () => {
    const { history } = this.props;

    history.push('/');
    this.setState({ hasError: false })
  }
}

export default withRouter(ErrorBoundary)