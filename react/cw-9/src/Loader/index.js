import React from 'react';
import { useSelector } from 'react-redux'

const Loader = () => {
    const loading = useSelector((store) => {
        return store.loading
    })

    return (
        loading && <div className="loader">
            <h3>Loading...</h3>
        </div>
    )
}

export default Loader;