import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import thunk from 'redux-thunk';

import { REQUEST_MOVIES_FAILURE, REQUEST_MOVIES_STARTED, REQUEST_MOVIES_SUCCESS } from './store/actions/types'

const initialStore = {
  loading: false,
}

const reducer = (store = initialStore, action) => {
  switch (action.type) {
    case REQUEST_MOVIES_STARTED:
      return {
        ...store,
        loading: true
      }
    case REQUEST_MOVIES_SUCCESS:
      return {
        ...store, 
        loading: false, 
        error: null,
        movies: [...store.movies, action.payload]
      }
    case REQUEST_MOVIES_FAILURE:
      return {
        ...store, 
        loading: false,
        error: action.payload
      }
    default:
      return store
  }
 
}

const store = createStore(reducer, applyMiddleware(thunk))

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
