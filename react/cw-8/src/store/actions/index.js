import { REQUEST_MOVIES_FAILURE, 
         REQUEST_MOVIES_SUCCESS, 
         REQUEST_MOVIES_STARTED 
} from './types'

import axios from 'axios';

export const getFilms = () => {
    return dispatch => {
        dispatch(getFilmsStarted())

        axios.get('https://swapi.dev/api/films/')
            .then((response) => {
                dispatch(getFilmsSuccess(response.data.results))
            }).catch( err => {
                dispatch(getFilmsFailure(err.message))
            })
    }
}

const getFilmsSuccess = ( films ) => ({   
        type: REQUEST_MOVIES_SUCCESS,
        payload: [...films]
    }
)

const getFilmsStarted = () => ({
    type: REQUEST_MOVIES_STARTED
})

const getFilmsFailure = ( error ) => ({
    type: REQUEST_MOVIES_FAILURE,
    payload: {
        ...error
    }
})
