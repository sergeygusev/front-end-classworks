import React, { Component } from 'react';
import axios from 'axios';
import { connect } from 'react-redux'
import MovieCard from '../MovieCard';
import { getFilms } from '../store/actions'

const mapStoreToProps = ({ movies }) => {
    return { movies: movies }
}

class MoviesList extends Component {

    render() {
        {console.log(this.props)}
        const { movies = [{title: 'No card'}] } = this.props
        return (
            <div className="movies">
                {movies.map((item) => (
                    <MovieCard film={item.title}></MovieCard>
                ))}
            </div>
        )
    }
}
export default connect(mapStoreToProps)(MoviesList) 