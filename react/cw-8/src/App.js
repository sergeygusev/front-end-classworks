import logo from './logo.svg';
import './App.css';
import MoviesList from './MoviesList'
import Loader from './Loader'

function App() {
  return (
    <>
      <Loader></Loader>
      <MoviesList></MoviesList>
    </>
  );
}

export default App;
