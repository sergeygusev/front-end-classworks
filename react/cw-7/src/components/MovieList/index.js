import React, {Component} from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import MovieCart from '../MovieCart';

const mapStoreToProps = ({ movies }) => {
    return {movies}
}

class MovieList extends Component {
    componentDidMount() {
        axios.get('https://swapi.dev/api/films/')
            .then((response) => {
                const dispatch = this.props.dispatch;
                dispatch({type: 'REQUEST_MOVIES', payload: response.data.results})
            })
    }

    render() {
        const {movies = []} = this.props;

        return (
            <div className="movies">
                {
                    movies.map((item) => (
                        <MovieCart film={item.title}/>
                    ))
                }
            </div>
        )
    }
}

export default connect(mapStoreToProps)(MovieList);