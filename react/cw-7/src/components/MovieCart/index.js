import React, {Component} from 'react';

const MovieCart = ({film}) => {
    return (
        <div className="movie-cart">
            <h1>{film}</h1>
        </div>
    )
}

export default MovieCart;