import "./App.css";
import CreateNotes from "./components/CreateNotes";
import ToDoList from "./components/ToDoList";

function App() {
  return (
    <div className="container">
      <h2>To do</h2>
      <CreateNotes></CreateNotes>
      <ToDoList></ToDoList>
    </div>
  );
}

export default App;
