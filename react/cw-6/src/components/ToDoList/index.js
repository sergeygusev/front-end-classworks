import React, { Component } from "react";
import { connect } from "react-redux";

const mapStoreToProps = ({ listOftodo }) => {
  return {
    list: listOftodo,
  };
};

class ToDoList extends Component {
  render() {
    const { list } = this.props;
    return (
      <div className="todo-list">
        {list.map(({ text }) => (
          <div className="todo-list__item">
            <span>{text}</span>
          </div>
        ))}
      </div>
    );
  }
}
ToDoList.defaultProps = {
  list: [{ text: "No todo" }],
};
export default connect(mapStoreToProps)(ToDoList);
