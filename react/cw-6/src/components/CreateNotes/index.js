import React, { Component } from "react";
import { connect } from "react-redux";

const mapStoreToProps = ({ user, code, listOftodo }) => {
  return {
    currentUser: user,
    code: code,
    listOftodo: listOftodo,
  };
};

const mapDispatchToProps = (dispatch) => {
    return {
        incrementCode: dispatch({ type: "INCREMENT_CODE" }),
        createToDo: (toDoText) => dispatch({ type: "CREATE_TODO", payload: { text: toDoText } })
    }
}

class CreateNotes extends Component {
  state = {
    toDoText: "First",
  };
  render() {
    const { toDoText } = this.state;
    const { currentUser, code, incrementCode, createToDo } = this.props;

    return (
      <div className="">
        <input
          type="text"
          value={toDoText}
          onChange={(e) => {
            this.setState({
              toDoText: e.target.value,
            });
          }}
        />
        <button
          onClick={
              () => {
                  createToDo(toDoText)
              }
          }
        >
          Create
        </button>
        <p>{currentUser}</p>
        <p>{code}</p>
        <button
          onClick={incrementCode}
        >
          Increment code
        </button>
      </div>
    );
  }
}

export default connect(mapStoreToProps, mapDispatchToProps)(CreateNotes);
