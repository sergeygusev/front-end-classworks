import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import redux, { createStore } from "redux";
import { Provider } from "react-redux";
import reportWebVitals from "./reportWebVitals";

const initialStore = {
  user: "Admin",
  code: 3310,
  listOftodo: [],
};

const reducer = (store = initialStore, action) => {
  switch (action.type) {
    case "INCREMENT_CODE":
      return { ...store, code: store.code + 1 };
    case "CREATE_TODO":
      return { ...store, listOftodo: [...store.listOftodo, action.payload] };
    default:
      return store;
  }
};

const store = createStore(
  reducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
