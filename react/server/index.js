const express = require('express');

const app = express();

const port = process.env.PORT || 8085;

const emails = [
    {
        "id": 1,
        "topic": "Email 1",
        "text": "Email 1 - Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum. In culpa qui officia deserunt mollit anim id est laborum."
    },
    {
        "id": 2,
        "topic": "Email 2",
        "text": "Email 2 - Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum. In culpa qui officia deserunt mollit anim id est laborum."
    },
    {
        "id": 3,
        "topic": "Email 3",
        "text": "Email 3 - Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum. In culpa qui officia deserunt mollit anim id est laborum."
    }
]

app.get('/api/emails', (req, res) => {
    res.send(emails)
});

app.get('/api/emails/:emailId', (req, res) => {
    const {emailId} = req.params;
    res.send(emails.find((email) => email.id === +emailId));
});

app.listen(port, () => {
    console.log(`Server listening on port ${port}`)
});