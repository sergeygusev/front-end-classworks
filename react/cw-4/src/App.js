import './App.css';
import { useState } from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import Login from "./components/Login";
import Home from "./components/Home";
import About from "./components/About";
import PrivateRoute from "./components/PrivatRoute";

function App() {
    const [isAuth, setAuth] = useState(false);

  return (
      <Router>
          <Switch>
              <PrivateRoute path="/home" isAuth={isAuth}>
                  <Home></Home>
              </PrivateRoute>
              <Route path="/about">
                  <About></About>
              </Route>
              <Route path="/">
                  <Login setAuth={setAuth}></Login>
              </Route>
          </Switch>
      </Router>
  );
}

export default App;
