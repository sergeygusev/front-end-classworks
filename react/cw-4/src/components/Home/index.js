import React from 'react';
import styles from "./styles";
import Smile from '../../icons/Smile';

const Home = () => {
    return (
        <section>
            <h1>Welcome, user!</h1>
        </section>
    );
};

export default Home;