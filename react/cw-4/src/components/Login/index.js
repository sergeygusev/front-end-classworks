import React from 'react';
import styles from "./styles";
import {Link} from 'react-router-dom';
import * as Icons from '../../icons/index';

const Login = ({setAuth}) => {
    return (
        <section>
            <form style={styles}>
                <input type="text"/>
                <input type="text"/>
                <button onClick={(e) => {
                    e.preventDefault();
                    setAuth(true)
                }}>Login</button>
                <Link to="/">Home</Link>
                {Icons.Smile({color: "red"})}
            </form>
        </section>
    );
};

export default Login;