import React, { createContext } from "react";
import "./styles/App.scss";
import HeaderMenu from "./components/HeaderMenu/HeaderMenu";
import AppRoutes from "./routes/AppRoutes";

const ContextData = createContext({
    user: "Admin",
    format: "BASE64"
});

function App() {
  return (
    <ContextData.Provider>
      <HeaderMenu />
      <AppRoutes />
    </ContextData.Provider>
  );
}

export default App;
