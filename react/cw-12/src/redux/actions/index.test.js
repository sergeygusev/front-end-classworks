import thunk from 'redux-thunk';
import configureStore from 'redux-mock-store';
import { getClothes } from "./data";

const openModal = () => ({ type: "OPEN_MODAL" });

const middlewares = [thunk];
const mockStore = configureStore(middlewares);

beforeEach(() => {

})

it('Check async action', () => {
    const defaultState = {};
    const store = mockStore(defaultState);

    store.dispatch(openModal());

    const actions = store.getActions();
    const expectedPayload = { type: "OPEN_MODAL" };

    expect(actions).toEqual([expectedPayload]);
});

it('Try fetch data from API', () => {
    const store = mockStore({});

    console.log(getClothes());
})