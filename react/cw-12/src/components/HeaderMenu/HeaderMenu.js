import React, { useContext } from "react";
import { NavLink } from "react-router-dom";

const HeaderMenu = () => {
    const userData = useContext(ContextData)
  return (
    <div className="header-menu">
      <NavLink
        exact
        data-item="Home"
        className="header-menu__link"
        to="/"
        activeClassName="header-menu__link_selected"
      >
        Home
      </NavLink>
      <NavLink
        exact
        data-item="Favourites"
        className="header-menu__link"
        to="/favourites"
        activeClassName="header-menu__link_selected"
      >
        Favourites
      </NavLink>
      <NavLink
        exact
        data-item="Bag"
        className="header-menu__link"
        to="/bag"
        activeClassName="header-menu__link_selected"
      >
        Bag
      </NavLink>
        <span>{userData}</span>
    </div>
  );
};

export default HeaderMenu;
