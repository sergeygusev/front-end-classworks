import { render, screen } from "@testing-library/react";
import React from "react";
import Button from "./Button";

test('Check btn', () => {
    const BTN_TEXT = 'Some text';
    render(<Button text={BTN_TEXT}></Button>);

    expect(screen.queryByText(BTN_TEXT)).toBeInTheDocument();
})