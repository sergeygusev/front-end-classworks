import React, { useEffect } from "react";
import PropTypes from "prop-types";
import Card from "../Card/Card";
import Modal from "../Modal/Modal";
import { useSelector, useDispatch } from "react-redux";
import {
  REMOVE_FROM_FAVOURITES,
  ADD_TO_FAVOURITES,
  OPEN_MODAL,
  CLOSE_MODAL,
} from "../../redux/actions/types";

const Bag = ({ clothId, setClothId }) => {
  const { cardsArr, openedModal, favorites, addedToBag } = useSelector(
    (store) => {
      return store;
    }
  );

  const dispatch = useDispatch();

  useEffect(() => {
    localStorage.setItem("favorites", JSON.stringify(favorites));
  });

  const toggleFavorites = (cardId) => {
    if (favorites.includes(cardId)) {
      dispatch({
        type: REMOVE_FROM_FAVOURITES,
        payload: favorites.filter((id) => id !== cardId),
      });
    } else {
      dispatch({
        type: ADD_TO_FAVOURITES,
        payload: [...favorites, cardId],
      });
    }
  };

  const addedToTheBagArr = [];

  const addToTheBagFunc = (item) => {
    addedToBag.forEach((code) => {
      if (item.code === code) {
        return addedToTheBagArr.push(item);
      }
    });
  };

  for (let i = 0; i < cardsArr.length; i++) {
    addToTheBagFunc(cardsArr[i]);
  }

  const removeFromTheBag = (cardId) => {
    const cardIndex = addedToBag.indexOf(cardId);
    delete addedToBag[cardIndex];
  };

  const listItems = addedToTheBagArr.map((cloth) => (
    <div className="card" key={cloth.code}>
      <Card
        toggleFavorites={toggleFavorites}
        cloth={cloth}
        filledStar={favorites.includes(cloth.code)}
        cardCross={true}
        openModal={() => {
          dispatch({ type: OPEN_MODAL });
        }}
        setClothId={setClothId}
      />
    </div>
  ));

  return (
    <div className="cards-container">
      {listItems}
      {openedModal && (
        <Modal
          header="Want to remove this item?"
          closeButton={true}
          text="This modal is made for confirming the remove of chosen item from your shopping bag. In order to remove this item press 'Ok', otherwise 'Cancel'"
          actions={
            <>
              <button
                onClick={() => dispatch({ type: "CLOSE_MODAL" })}
                style={{ backgroundColor: "white" }}
                className="modal__main-part-btn"
              >
                Cancel
              </button>
              <button
                onClick={(e) => {
                  dispatch({ type: CLOSE_MODAL });
                  removeFromTheBag(clothId);
                }}
                style={{ backgroundColor: "white" }}
                className="modal__main-part-btn"
              >
                Ok
              </button>
            </>
          }
          mainBg="white"
          headerBg="white"
          closeModal={() => {
            dispatch({ type: CLOSE_MODAL });
          }}
        />
      )}{" "}
    </div>
  );
};

Bag.propTypes = {
  clothId: PropTypes.string.isRequired,
};

export default Bag;
