import React from 'react';
import './App.css';
import CpOne from "./components/cpOne";

class App extends React.Component {
    state = {
        counter: 0
    }

    counterHandler = (newData) => {
        this.setState((state) => (
            {
                counter: newData
            }
        ))
    }

  render() {
      return (
          <div className="test">
              <span className="counter">{this.state.counter}</span>
              <CpOne data={this.state.counter} dataHandler={this.counterHandler}>
                  <span>Some child</span>
              </CpOne>
              <button onClick={this.counterHandler}>Test</button>
          </div>
      );
  }
}

export default App;
