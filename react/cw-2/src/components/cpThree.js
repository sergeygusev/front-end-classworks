import React, { Component } from 'react';
import PropTypes from "prop-types";

class CpThree extends Component {
    btnClick = () => {
        this.props.dataHandler(this.props.data + 3);
    }

    render() {
        const { data, dataHandler } = this.props;

        return (
            <div className="cp cp_three">
                <p>{data}</p>
                <div>cp three</div>
                <button className="btn" onClick={this.btnClick}>Trigger</button>
                <button className="btn" onClick={() => {
                    dataHandler(data + 3)
                }}>Trigger</button>
            </div>
        )
    }
}

CpThree.defaultProps = {
    data: 100
}

CpThree.propTypes = {
    data: PropTypes.number
}

export default CpThree;