import React, { Component } from 'react';
import CpThree from "./cpThree";

class CpTwo extends Component {
    render() {
        const { data, dataHandler } = this.props;
        return (
            <div className="cp cp_two">
                <div>cp two</div>
                <CpThree data={data} dataHandler={dataHandler} />
            </div>
        )
    }

}

export default CpTwo;