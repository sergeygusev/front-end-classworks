import React, { Component } from 'react';
import CpTwo from "./cpTwo";

class CpOne extends Component {
    render() {
        const { data, dataHandler, children } = this.props;

        return (
            <div className="cp cp_one">
                <span>cp one</span>
                <p>{children}</p>
                <CpTwo data={data} dataHandler={dataHandler} />
            </div>
        )
    }
}

export default CpOne;